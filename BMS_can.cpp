#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <iostream>
#include <bitset>

using namespace std;
int s;

can_frame receive()
{

	can_frame frame_rx;

	int nbytes = read(s, &frame_rx, sizeof(struct can_frame));

	return frame_rx;
}

int main()
{

	int ret;
	int ret1, ang;
	// int flag=0;

	struct sockaddr_can addr;
	struct ifreq ifr;
	struct can_frame frame;
	struct can_frame frame2;
	float RPS;

	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
	{
		perror("Socket");
		return 1;
	}

	strcpy(ifr.ifr_name, "can0");
	ioctl(s, SIOCGIFINDEX, &ifr);

	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
	{
		perror("Bind");
		return 1;
	}

	while (1)
	{

		frame2 = receive();

		if (((frame2.can_id & CAN_EFF_FLAG) ? (frame2.can_id & CAN_EFF_MASK) : (frame2.can_id & CAN_SFF_MASK)) == 0XF462F3) // 1
		{

			int status = frame2.data[1] & 0x00111000;
			status = status >> 3;

			if (status == 0)
			{
				printf("no Load connected\n");
			}

			else if (status == 1)
			{
				printf(" Load connected\n");
			}
			else if (status == 2)
			{
				printf("Charger connected\n");
			}

			else if (status == 3)
			{
				printf("Discharging\n");
			}
			else if (status == 4)
			{
				printf("Charging\n");
			}

			int Discharge_Sw_status = frame2.data[1] & 0x00000100;
			Discharge_Sw_status = Discharge_Sw_status >> 2;

			printf("Discharge Switch Status= %d\n", Discharge_Sw_status);

			int charge_Sw_status = frame2.data[1] & 0x00000010;
			charge_Sw_status = charge_Sw_status >> 1;

			printf("Charge Switch Status= %d\n", charge_Sw_status);

			int lsb = frame2.data[4];
			int msb = frame2.data[5];
			float combined = (msb << 8) | lsb;
			printf("\nVoltage %f\n", combined / 100);

			// printf("%x\n    ", combined / 100);

			int lsb1 = frame2.data[2];
			int msb1 = frame2.data[3];
			int combined1 = (msb1 << 8) | lsb1;
			printf("\nCurrent %d\n", combined1 / 100);

			int fault_alert0 = frame2.data[6] & 0x10000000;
			fault_alert0 = fault_alert0 >> 7;
			printf("Short Circuit flag %d\n", fault_alert0);

			int fault_alert1 = frame2.data[6] & 0x01000000;
			fault_alert1 = fault_alert1 >> 6;
			printf("Cell_over_voltage flag %d\n", fault_alert1);

			int fault_alert2 = frame2.data[6] & 0x00100000;
			fault_alert2 = fault_alert2 >> 5;
			printf("cell Under voltage flag %d\n", fault_alert2);

			int fault_alert3 = frame2.data[6] & 0x00010000;
			fault_alert3 = fault_alert3 >> 4;
			printf("Over Current Discharging flag %d\n", fault_alert3);

			int fault_alert4 = frame2.data[6] & 0x00001000;
			fault_alert4 = fault_alert4 >> 3;
			printf("Over Temperature flag %d\n", fault_alert4);

			int fault_alert5 = frame2.data[6] & 0x00000001;
			fault_alert5 = fault_alert5 >> 0;
			printf("Over Charge Current flag %d\n", fault_alert5);

			////////////////////////////////////////////////////////////////////////////////////////////////
			int fault_alert8 = frame2.data[7] & 0x10000000;
			fault_alert8 = fault_alert8 >> 7;
			printf("over current cont discharging flag%d\n", fault_alert8);

			int fault_alert9 = frame2.data[7] & 0x01000000;
			fault_alert9 = fault_alert9 >> 6;
			printf("Under Temperature flag %d\n", fault_alert9);

			int fault_alert10 = frame2.data[7] & 0x00100000;
			fault_alert10 = fault_alert10 >> 5;
			printf("Over Temparature_Board flag %d\n", fault_alert10);

			int fault_alert11 = frame2.data[7] & 0x00010000;
			fault_alert11 = fault_alert11 >> 4;
			printf("Under Temperature Board flag %d\n", fault_alert11);

			int fault_alert12 = frame2.data[7] & 0x00001000;
			fault_alert12 = fault_alert12 >> 3;
			printf("Cell_CoNN flag %d\n", fault_alert12);

			int fault_alert13 = frame2.data[7] & 0x00000100;
			fault_alert13 = fault_alert13 >> 2;
			printf("PRL_Fault %d\n", fault_alert13);

			int fault_alert14 = frame2.data[7] & 0x00000010;
			fault_alert14 = fault_alert14 >> 1;
			printf("Cell_Diff_Fault  %d\n", fault_alert14);

			int fault_alert15 = frame2.data[7] & 0x00000001;
			fault_alert15 = fault_alert15 >> 0;
			printf("Pack_OV_Fault  %d\n", fault_alert15);
		}

		if (((frame2.can_id & CAN_EFF_FLAG) ? (frame2.can_id & CAN_EFF_MASK) : (frame2.can_id & CAN_SFF_MASK)) == 0x1FF462F3)
		{

			if (frame2.data[0] == 0x0E) // 2
			{

				int soh = frame2.data[1];
				printf("Soh  %d\n", soh);

				int soc = frame2.data[2];
				printf("Soc  %d\n", soc);

				int cyclelsb = frame2.data[6];
				int cyclemsb = frame2.data[7];
				int cycles = (cyclemsb << 8) | cyclelsb;
				printf("\n Cycles %d\n", cycles);
			}

			if ((frame2.data[0] == 0x06) && (frame2.data[1] == 0x01)) // 3
			{

				int T0 = frame2.data[2];
				printf("T0  %d\n", T0);

				int T1 = frame2.data[3];
				printf("T1  %d\n", T1);

				int T2 = frame2.data[4];
				printf("T2  %d\n", T2);
			}

			if ((frame2.data[0] == 0x0A) && (frame2.data[1] == 0x00)) // 4
			{

				int cell_count = frame2.data[3];
				printf("Cell Count  %d\n", cell_count);
			}

			if ((frame2.data[0] == 0x0A) && (frame2.data[1] == 0x01)) // 5
			{

				int C1Lsb = frame2.data[2];
				int C1Msb = frame2.data[3];
				float C1 = (C1Msb << 8) | C1Lsb;
				printf("\n Cell 1 Voltage %f\n", C1 / 1000);

				int C2Lsb = frame2.data[4];
				int C2Msb = frame2.data[5];
				float C2 = (C2Msb << 8) | C2Lsb;
				printf("\n Cell 2 Voltage %f\n", C2 / 1000);

				int C3Lsb = frame2.data[6];
				int C3Msb = frame2.data[7];
				float C3 = (C3Msb << 8) | C3Lsb;
				printf("\n Cell 3 Voltage %f\n", C3 / 1000);
			}

			if ((frame2.data[0] == 0x0A) && (frame2.data[1] == 0x02)) // 6
			{

				int C4Lsb = frame2.data[2];
				int C4Msb = frame2.data[3];
				float C4 = (C4Msb << 8) | C4Lsb;
				printf("\n Cell 4 Voltage %f\n", C4 / 1000);

				int C5Lsb = frame2.data[4];
				int C5Msb = frame2.data[5];
				float C5 = (C5Msb << 8) | C5Lsb;
				printf("\n Cell 5 Voltage %f\n", C5 / 1000);

				int C6Lsb = frame2.data[6];
				int C6Msb = frame2.data[7];
				float C6 = (C6Msb << 8) | C6Lsb;
				printf("\n Cell 6 Voltage %f\n", C6 / 1000);
			}

			if ((frame2.data[0] == 0x0A) && (frame2.data[1] == 0x03)) // 7
			{

				int C7Lsb = frame2.data[2];
				int C7Msb = frame2.data[3];
				float C7 = (C7Msb << 8) | C7Lsb;
				printf("\n Cell 7 Voltage %f\n", C7 / 1000);

				int C8Lsb = frame2.data[4];
				int C8Msb = frame2.data[5];
				float C8 = (C8Msb << 8) | C8Lsb;
				printf("\n Cell 8 Voltage %f\n", C8 / 1000);

				int C9Lsb = frame2.data[6];
				int C9Msb = frame2.data[7];
				float C9 = (C9Msb << 8) | C9Lsb;
				printf("\n Cell 9 Voltage %f\n", C9 / 1000);
			}

			if ((frame2.data[0] == 0x0A) && (frame2.data[1] == 0x04)) // 8
			{

				int C10Lsb = frame2.data[2];

				int C10Msb = frame2.data[3];

				float C10 = (C10Msb << 8) | C10Lsb;

				printf("\n Cell 10 Voltage %f\n", C10 / 1000);
			}

			///////////////////////////////////////////////////////////////////////////////////////////
			if (frame2.data[0] == 0x36) // 9
			{
				int S11 = frame2.data[7] & 0x10000000;
				S11 = S11 >> 7;
				printf("Cell 1 Balancing State%d\n", S11);

				int S12 = frame2.data[7] & 0x01000000;
				S12 = S12 >> 6;
				printf("Cell 2 Balancing State %d\n", S12);

				int S13 = frame2.data[7] & 0x00100000;
				S13 = S13 >> 5;
				printf("Cell 3 Balancing State %d\n", S13);

				int S14 = frame2.data[7] & 0x00010000;
				S14 = S14 >> 4;
				printf("Cell 4 Balancing State %d\n", S14);

				int S15 = frame2.data[7] & 0x00001000;
				S15 = S15 >> 3;
				printf("Cell 5 Balancing State %d\n", S15);

				int S16 = frame2.data[7] & 0x00000100;
				S16 = S16 >> 2;
				printf("Cell 6 Balancing State %d\n", S16);

				int S17 = frame2.data[7] & 0x00000010;
				S17 = S17 >> 1;
				printf("Cell 7 Balancing State  %d\n", S17);

				int S18 = frame2.data[7] & 0x00000001;
				S18 = S18 >> 0;
				printf("Cell 8 Balancing State  %d\n", S18);
				/////////////////////////////////////////////////////////////
				int S21 = frame2.data[7] & 0x10000000;
				S21 = S21 >> 7;
				printf("Cell 9 Balancing State%d\n", S21);

				int S22 = frame2.data[7] & 0x01000000;
				S22 = S22 >> 6;
				printf("Cell 10 Balancing State %d\n", S22);

				int S23 = frame2.data[7] & 0x00100000;
				S23 = S23 >> 5;
				printf("Cell 11 Balancing State %d\n", S23);

				int S24 = frame2.data[7] & 0x00010000;
				S24 = S24 >> 4;
				printf("Cell 12 Balancing State %d\n", S24);

				int S25 = frame2.data[7] & 0x00001000;
				S25 = S25 >> 3;
				printf("Cell 13 Balancing State %d\n", S25);

				int S26 = frame2.data[7] & 0x00000100;
				S26 = S26 >> 2;
				printf("Cell 14 Balancing State %d\n", S26);

				int S27 = frame2.data[7] & 0x00000010;
				S27 = S27 >> 1;
				printf("Cell 15 Balancing State  %d\n", S27);

				int S28 = frame2.data[7] & 0x00000001;
				S28 = S28 >> 0;
				printf("Cell 16 Balancing State  %d\n", S28);
				//////////////////////////////////////////////////////////////////////////////

				int S31 = frame2.data[7] & 0x10000000;
				S31 = S31 >> 7;
				printf("Cell 17 Balancing State  %d\n", S31);

				int S32 = frame2.data[7] & 0x01000000;
				S32 = S32 >> 6;
				printf("Cell 18 Balancing State %d\n", S32);

				int S33 = frame2.data[7] & 0x00100000;
				S33 = S33 >> 5;
				printf("Cell 19 Balancing State %d\n", S33);

				int S34 = frame2.data[7] & 0x00010000;
				S34 = S34 >> 4;
				printf("Cell 20 Balancing State %d\n", S34);

				int S35 = frame2.data[7] & 0x00001000;
				S35 = S35 >> 3;
				printf("Cell 21 Balancing State %d\n", S35);

				int S36 = frame2.data[7] & 0x00000100;
				S36 = S36 >> 2;
				printf("Cell 22 Balancing State %d\n", S36);

				int S37 = frame2.data[7] & 0x00000010;
				S37 = S37 >> 1;
				printf("Cell 23 Balancing State  %d\n", S37);

				int S38 = frame2.data[7] & 0x00000001;
				S38 = S38 >> 0;
				printf("Cell 24 Balancing State  %d\n", S38);
				//////////////////////////////////////////////////////////////////////////////////////////

				int S41 = frame2.data[7] & 0x10000000;
				S41 = S41 >> 7;
				printf("Cell 25 Balancing State %d\n", S41);

				int S42 = frame2.data[7] & 0x01000000;
				S42 = S42 >> 6;
				printf("Cell 26 Balancing State %d\n", S42);

				int S43 = frame2.data[7] & 0x00100000;
				S43 = S43 >> 5;
				printf("Cell 27 Balancing State %d\n", S43);

				int S44 = frame2.data[7] & 0x00010000;
				S44 = S44 >> 4;
				printf("Cell 28 Balancing State %d\n", S44);

				int S45 = frame2.data[7] & 0x00001000;
				S45 = S45 >> 3;
				printf("Cell 29 Balancing State %d\n", S45);

				int S46 = frame2.data[7] & 0x00000100;
				S46 = S46 >> 2;
				printf("Cell 30 Balancing State %d\n", S46);

				int S47 = frame2.data[7] & 0x00000010;
				S47 = S47 >> 1;
				printf("Cell 31 Balancing State  %d\n", S47);

				int S48 = frame2.data[7] & 0x00000001;
				S48 = S48 >> 0;
				printf("Cell 32 Balancing State  %d\n", S48);
			}

			if (frame2.data[0] == 0x14) // 10
			{

				int Chemistry = frame2.data[1];
				if (Chemistry == 0)
				{
					printf("NMC\n");
				}
				else if (Chemistry == 1)
				{
					printf("NMC\n");
				}
				else if (Chemistry == 2)
				{
					printf("LFP\n");
				}

				int Capacity = frame2.data[2];
				printf("Capacity (mAh) %d\n", Capacity * 100);
			}

			if (frame2.data[0] == 0x70) // 11
			{

				int year = frame2.data[2] & 0xF0;

				cout << bitset<8>(year) << endl;
				int year1 = year >> 4;

				printf("year:(20 to 35) %d\n", year1);

				///////////////////////////////////////////////////////////////////////////////////////////

				int Month = frame2.data[2] & 0x0F;

				cout << bitset<8>(Month) << endl;

				printf("Month:(1 to 12) %d\n", Month);

				int Date = frame2.data[3];
				printf("Date:(1 to 31) %d\n", Date);

				int Hour = frame2.data[4];
				printf("Hour:(1 to 24) %d\n", Hour);

				int Minute = frame2.data[5];
				printf("Minute:(1 to 24) %d\n", Minute);

				int Second = frame2.data[6];
				printf("Second:(1 to 24) %d\n", Second);

				int Subsecond = frame2.data[7];
				printf("Subsecond:(1 to 24) %d\n", Subsecond);
			}

			if (frame2.data[0] == 0x39)
			{ // 12

				int Dsg_Fet = frame2.data[1];
				printf("Dsg_fet %d\n", Dsg_Fet);

				int Chg_Fet = frame2.data[2];
				printf("Chg_Fet %d\n", Chg_Fet);

				int Fet_control_Mode = frame2.data[3];
				printf("Fet_control_Mode %d\n", Fet_control_Mode);
			}
		}
	}

	return 0;
}
